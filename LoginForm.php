<?php
  session_start();
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<!--Menampilkan navigasi terhadap bagian header website -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">BMNC WEBSITE</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Home.php">Home</a></li>
      <li class="active"><a href="LoginForm.php">Login</a></li>
    </ul>
  </div>
</nav>
<!-- End -->
<!--Menampilkan form login -->
<div class="container">
   <?php
    if(isset($_SESSION['wrong_credential'])){ //bernilai true apabila user salah memasukkan username atau password
      echo "<h3> Maaf, username atau password yang anda masukkan salah.</h3>";
      session_unset();
      session_destroy();
    }
    if(isset($_SESSION['wrong-role'])){ //bernilai true apabila user belum login
      echo "<h3>Anda harus login terlebih dahulu</h3>";
      session_unset();
      session_destroy();
    }
    if (isset($_SESSION['username'])) { //mengecek apakah sebelumnya user telah melakukan login. Apabila user sebelum nya telah login, artinya redirection ke halaman ini terjadi karena user menekan tombol logout
      session_unset();
      session_destroy();
      echo "<h3>Anda telah berhasil logout</h3>";
    }
  ?>
  <h2>Login</h2>
  <form action="Home.php" method="post">
    <div class="form-group">
      <label for="username">Username:</label>
      <input type="text" name="username" class="form-control" id="username" placeholder="Enter username" required>
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" name="password" class="form-control" id="pwd" placeholder="Enter password" required>
    </div>
    <div class="checkbox">
      <label><input type="checkbox"> Remember me</label>
    </div>
    <input type="submit" class="btn btn-default" value="Login">
  </form>
</div>
<!-- End -->

</body> 
</html>
