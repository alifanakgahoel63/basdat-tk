<?php
  session_start();
  if(isset($_POST['username']) && !isset($_SESSION["username"])){ //bernilai true apabila user dibawa ke halaman ini melalui tombol login
  	if($_POST['username'] == 'admin' || $_POST['username'] == 'narasumber'){ //mengecek apakah username yang dimasukkan sudah benar
  		if($_POST['password'] == '123'){ //mengecek apakah password yang dimasukkan sudah benar
  			$_SESSION["username"] = $_POST["username"];
    		$_SESSION["password"] = $_POST["password"];
  		}
  		else{
  			$_SESSION['wrong_credential'] = true; //menandakan bahwa user telah salah memasukkan username atau password
  			header("Location: LoginForm.php"); //kembali ke halaman LoginForm.php
  		}
  	}
  	else{
  		$_SESSION['wrong_credential'] = true; //menandakan bahwa user telah salah memasukkan username atau password
  		header("Location: LoginForm.php"); //kembali ke halaman LoginForm.php
  	}
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<!--Menampilkan navigasi terhadap bagian header website -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">BMNC WEBSITE</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="Home.php">Home</a></li>
      <?php
        if(isset($_SESSION['username'])){
            if($_SESSION["username"] == 'narasumber' || $_SESSION['username'] == 'admin'){
              echo '<li><a href="Profil.php">Profil</a></li>';
              if($_SESSION['username'] == 'narasumber'){
                  echo '<li><a href="Berita.php">Berita</a></li>
                  <li><a href="ViewPolling.php">Lihat Polling</a></li>
                  <li><a href="PollingBerita.php">Polling Berita</a></li>
                  <li><a href="PollingBiasa.php">Polling Biasa</a></li>
                  <li><a href="RegistrationForm.php">Registration</a></li>';
              }
            echo'<li><a href="LoginForm.php">Logout</a></li>';
          }
        }
        else{
          echo '<li><a href="LoginForm.php">Login</a></li>';
        }
      ?>
    </ul>
  </div>
</nav>
<!-- End -->

<?php
  if(isset($_SESSION['username'])){
    echo "<h1>Hello ".$_SESSION["username"]."!</h1>";
  }
?>
<h1>This is our main page</h1>

</body> 
</html>