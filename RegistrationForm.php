<?php
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['username'] != 'narasumber') { //bernilai true apabila user tidak dalam keadaan login atau login tetapi tidak sebagai narasumber
    $_SESSION['wrong-role'] = true;
    header("Location: LoginForm.php"); //kembali ke halaman LoginForm.php
  }

?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  	$(document).ready(function(){
  		init();
  		function init(){
	  		var childToAppend = "<label for = 'status'>Status Kemahasiswaan : </label>";
	  		childToAppend += "<input type = 'text' class = 'form-control' id = 'status' placeholder = 'Enter Status Kemahasiswaan'>";
	  		$("#additional-form").append(childToAppend);
	  	}
  	});
  	function deleteChild(){
	  	$("#additional-form").empty();
	}
  	function tampilkanStatus(){
  		deleteChild();
  		var childToAppend = "<label for = 'status'>Status Kemahasiswaan : </label>";
  		childToAppend += "<input type = 'text' class = 'form-control' id = 'status' placeholder = 'Enter Status Kemahasiswaan'>";
  		$("#additional-form").append(childToAppend);
  	}
  	function tampilkanIdUniv(){
  		deleteChild();
  		$("#additional-form").append("<label for = 'id_univ'>ID Universitas : </label>");
  		$("#additional-form").append("<select name = 'id_univ' class = 'form-control' id = 'id_univ'>");
  		for (var i = 1; i <= 50; i++) {
  			$("#id_univ").append("<option value '" + i + "'>" + i + "</option>");
  		}
  		$("#additional-form").append("</select>");
  	}
  	function adjust(){
  		var x = document.getElementById("role").selectedIndex; //x berisi index pada child element <select>. Ex : mahasiswa berada pada index 0, staf berada pada index 1.
  		var y = document.getElementById("role").options; //y berisi htmlOptionsCollection (mirip seperti array). Isinya adalah object-object option pada child select. Object ini merepresentasikan informasi-informasi yang terdapat pada masing-masing option, seperti panjang karakter isi option (ex : Staf, Mahasiswa, Dosen, Admin), dsb.
  		var option = y[x].text;
  		if (option == 'Mahasiswa') {
  			tampilkanStatus();
  		}
  		else if(option == 'Staf' || option == 'Dosen'){
  			tampilkanIdUniv();
  		}
  		else{
  			deleteChild();
  		}
  	}
  	
  </script>
</head>
<body>

<!--Menampilkan navigasi terhadap bagian header website -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">BMNC WEBSITE</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Home.php">Home</a></li>
      <li><a href="Profil.php">Profil</a></li>
      <li><a href="Berita.php">Berita</a></li>
      <li><a href="ViewPolling.php">Lihat Polling</a></li>
      <li><a href="PollingBerita.php">Polling Berita</a></li>
      <li><a href="PollingBiasa.php">Polling Biasa</a></li>
      <li class = "active"><a href="RegistrationForm.php">Registration</a></li>
      <li><a href="LoginForm.php">Logout</a></li>
    </ul>
  </div>
</nav>
<!-- End -->

<!--Menampilkan form registrasi -->
<div class="container">
  <h2>Registration Form</h2>
  <form>
    <div class="form-group">
      <label for="role">Role:</label>
      <select name="role" class="form-control" id = "role" onchange="adjust()">
        <option value="mahasiswa">Mahasiswa</option>
        <option value="staf">Staf</option>
        <option value="dosen">Dosen</option>
        <option value="admin">Admin</option>
      </select>
    </div>

    <div class="form-group">
      <label for="username">Username:</label>
      <input type="text" class="form-control" id="username" placeholder="Enter username">
    </div>

    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password">
    </div>

    <div class="form-group">
      <label for="identitas">Nomor Identitas(NPM, NIK):</label>
      <input type="text" class="form-control" id="identitas" placeholder="Enter identity number">
    </div>

    <div class="form-group">
      <label for="birthplace">Tempat lahir:</label>
      <input type="text" class="form-control" id="birthplace" placeholder="Enter Birthplace">
    </div>

    <div class="form-group">
      <label for="birthdate">Tanggal lahir:</label>
      <input type="text" class="form-control" id="birthdate" placeholder="Enter Birthdate">
    </div>

    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter Email">
    </div>

    <div class="form-group">
      <label for="phonenumber">Nomor HP:</label>
      <input type="tel" class="form-control" id="phonenumber" placeholder="Enter Phone Number">
    </div>

    <div class="form-group" id = "additional-form">
      
    </div>

    <div class="checkbox">
      <label><input type="checkbox"> Remember me</label>
    </div>

    <button type="submit" class="btn btn-default">Login</button>
  </form>
</div>
<!-- End -->

</body> 
</html>
