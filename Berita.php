<?php
  session_start();
  if (!isset($_SESSION['username']) || $_SESSION['username'] != 'narasumber') { //bernilai true apabila user tidak dalam keadaan login atau login tetapi tidak sebagai narasumber
    $_SESSION['wrong-role'] = true;
    header("Location: LoginForm.php"); //kembali ke halaman LoginForm.php
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="TK 4">
	<meta name="author" content="Kelompok 1 Basis Data D">
	<style type="text/css">
		footer{
				color:blue
				position: fixed;
				left: 0;
				bottom: 0;
				width: 100%;
				background-color: red;
				color: white;
				text-align: center;
			}
		span.blue{
				color:blue
			}	
		span.red{
				color:red
			}	
	</style>	
    <!-- bootstrap csss -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">    
	
    <title> 
        Biru Merah News Corporation
    </title>
</head>

<body style="background-color:white">
    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">BMNC WEBSITE</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li><a href="Home.php">Home</a></li>
	      <li><a href="Profil.php">Profil</a></li>
	      <li class="active"><a href="Berita.php">Berita</a></li>
	      <li><a href="ViewPolling.php">Lihat Polling</a></li>
	      <li><a href="PollingBerita.php">Polling Berita</a></li>
          <li><a href="PollingBiasa.php">Polling Biasa</a></li>
	      <li><a href="RegistrationForm.php">Registration</a></li>
	      <li><a href="LoginForm.php">Logout</a></li>
	    </ul>
	  </div>
	</nav>
    <content>
        <div class="container">
			<h1><b>
				<center><span class="blue">Biru</span> <span class="red">Merah</span><span style="color:purple"> News Corporation</span></center>
			</b></h1>
			<h2> Tambah Berita</h2>
            <!--{% block content %}-->
             <!-- content goes here -->
            <!--{% endblock %}-->
        </div>
		<form class="form-horizontal">
			<div class="form-group">
		<center>
				<label class="control-label col-sm-2" for="waktu">Judul:</label>
				<div class="col-sm-10">
					<textarea class="form-control" rows="1" id="berita" name="Judul"></textarea>
				</div>
				<label class="control-label col-sm-2" for="waktu">URL:</label>
				<div class="col-sm-10">
					<textarea class="form-control" rows="1" id="berita" name="URL"></textarea>
				</div>
				<label class="control-label col-sm-2" for="waktu">Topik:</label>
				<div class="col-sm-10">
					<textarea class="form-control" rows="1" id="berita" name="Topik"></textarea>
				</div>
				<label class="control-label col-sm-2" for="waktu">Jumlah Kata:</label>
				<div class="col-sm-10">
					<textarea class="form-control" rows="1" id="berita" name="Jumlah Kata"></textarea>
				</div>
				<label class="control-label col-sm-2" for="waktu">Tag:</label>
				<div class="col-sm-10">
					<textarea class="form-control" rows="3" id="berita" name="Tag"></textarea>
				</div>
		</center>	
			</div>
			<div class="btn-container">
			<center>
				<button type="button" class="btn btn-primary">Submit</button>
			</center>
			</div>
			
		</form>

    </content>

    <!-- Jquery n Bootstrap Script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
