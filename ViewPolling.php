<?php
  session_start();
  if (!isset($_SESSION['username'])) { //bernilai true apabila user tidak dalam keadaan login
    $_SESSION['wrong-role'] = true;
    header("Location: LoginForm.php"); //kembali ke halaman LoginForm.php
  }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Halaman Polling</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<!--Menampilkan navigasi terhadap bagian header website -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">BMNC WEBSITE</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Home.php">Home</a></li>
      <?php
        if(isset($_SESSION['username'])) {
            if($_SESSION["username"] == 'narasumber' || $_SESSION['username'] == 'admin') {
              echo '<li><a href="Profil.php">Profil</a></li>';
              if($_SESSION['username'] == 'narasumber'){
                  echo '<li><a href="Berita.php">Berita</a></li>
                  <li class = "active"><a href="ViewPolling.php">Lihat Polling</a></li>
                  <li><a href="PollingBerita.php">Polling Berita</a></li>
                  <li><a href="PollingBiasa.php">Polling Biasa</a></li>
                  <li><a href="RegistrationForm.php">Registration</a></li>';
              }
            echo'<li><a href="LoginForm.php">Logout</a></li>';
          }
        }
        else{
          echo '<li><a href="LoginForm.php">Login</a></li>';
        }
      ?>
    </ul>
  </div>
</nav>
<!-- End -->

<div class="container">
  <h2>Daftar Polling</h2>       
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Nomor</th>
        <th>Judul Polling</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Mahasiswa Terbang</td>
      </tr>
      <tr>
        <td>2</td>
        <td>Penelitian Gaib</td>
      </tr>
      <tr>
        <td>3</td>
        <td>Ruang Kelas Sepi</td>
      </tr>
    </tbody>
  </table>
</div>
</body>
</html>