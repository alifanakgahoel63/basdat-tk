<?php
  session_start();
  if (!isset($_SESSION['username'])) { //bernilai true apabila user tidak dalam keadaan login
    $_SESSION['wrong-role'] = true;
    header("Location: LoginForm.php"); //kembali ke halaman LoginForm.php
  }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Profil</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>

<!--Menampilkan navigasi terhadap bagian header website -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">BMNC WEBSITE</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Home.php">Home</a></li>
      <?php
        if(isset($_SESSION['username'])) {
            if($_SESSION["username"] == 'narasumber' || $_SESSION['username'] == 'admin') {
              echo '<li class = "active"><a href="Profil.php">Profil</a></li>';
              if($_SESSION['username'] == 'narasumber'){
                  echo '<li><a href="Berita.php">Berita</a></li>
                  <li><a href="ViewPolling.php">Lihat Polling</a></li>
                  <li><a href="PollingBerita.php">Polling Berita</a></li>
                  <li><a href="PollingBiasa.php">Polling Biasa</a></li>
                  <li><a href="RegistrationForm.php">Registration</a></li>';
              }
            echo'<li><a href="LoginForm.php">Logout</a></li>';
          }
        }
        else{
          echo '<li><a href="LoginForm.php">Login</a></li>';
        }
      ?>
    </ul>
  </div>
</nav>
<!-- End -->

<div class="container">
  <h2>Halaman Profil</h2>
  <?php
  if(isset($_SESSION['username'])){
    echo "<p>Halaman profil ".$_SESSION["username"]."</p>";
  }
?>
           
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Role</th>
        <?php
        if($_SESSION["username"] == 'narasumber') {
            echo '<td>Narasumber</td>';
        } else {
          echo '<td>Admin</td>';
        }
        ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>Nomor Identitas</th>
        <td>1902210294</td>
      </tr>
      <tr>
        <th>Tempat lahir</th>
        <td>Depok</td>
      </tr>
      <tr>
        <th>Tanggal lahir</th>
        <td>20 Agustus 2010</td>
      </tr>
      <tr>
        <th>Email</th>
        <td>agus@mail.com</td>
      </tr>
      <tr>
        <th>Nomor HP</th>
        <td>082192002812</td>
      </tr>
      <tr>
      <?php
        if(isset($_SESSION['role']) && $_SESSION["role"] == 'staf') {
            echo '<th>ID Universitas</th><br><td>1</td>';
        } else {
          echo '<th>Status kemahasiswaan</th><br><td>Aktif</td>';
        }
      ?>
      </tr>
    </tbody>
  </table>
</div>
  <div class="container">
  <h2>List Artikel</h2>
  <?php
  if(isset($_SESSION['username'])){
    echo "<p>Halaman profil ".$_SESSION["username"]."</p>";
  }
?>        
  <table class="table">
    <thead>
      <tr>
        <th>Nomor</th>
        <th>Judul Artikel</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td data-toggle="modal" data-target="#modalForm">Mahasiswa Terbang</td>
</button>
      </tr>
      <tr>
        <td>2</td>
        <td data-toggle="modal" data-target="#modalForm">Penelitian Gaib</td>
      </tr>
      <tr>
        <td>3</td>
        <td data-toggle="modal" data-target="#modalForm">Ruang Kelas Sepi</td>
      </tr>
    </tbody>
  </table>
</div>
  <!-- Modal -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Tutup</span>
                </button>
                <h4 class="modal-title" id="labelModalKu">Contact Form</h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Judul</th>
                  <th>Topik</th>
                  <th>Jumlah kata</th>
                  <th>Rata-rata rating</th>
                  <th>ID Universitas</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Mahasiswa Terbang</td>
                  <td>Mahasiswa</td>
                  <td>1000</td>
                  <td>10.0</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>Penelitian Gaib</td>
                  <td>Riset</td>
                  <td>2000</td>
                  <td>9.0</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>Ruang Kelas Sepi</td>
                  <td>Pendidikan</td>
                  <td>3000</td>
                  <td>8.0</td>
                  <td>3</td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</div>
    
</body>
</html>
